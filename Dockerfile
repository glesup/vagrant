FROM centos:latest
RUN yum install -y yum-utils && yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo && yum -y install vagrant && yum -y install rubygems && dnf group install -y "Development Tools" && dnf install -y ruby-devel
RUN vagrant plugin install vagrant-vsphere && gem install nokogiri
